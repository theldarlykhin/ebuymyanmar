package com.hnttechs.www.ebuymyanmar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dell on 5/7/16.
 */
public class AllCategoryFragment extends Fragment {

    HeaderGridView gridView;
    ImageView slidingimage;
    static ArrayList<HashMap<String, String>> arraylist;
    ListViewAdapter adapter;
    String website_name;
    SyncDataFromServer.allcategory all_category;
    ImageLoader imageLoader = new ImageLoader(getActivity());
    static int currentimageindex = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_all_category, container, false);

        gridView = (HeaderGridView) rootView.findViewById(R.id.gridView1);

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View headerView = layoutInflater.inflate(R.layout.homescreen_gridview_header, null);
        slidingimage = (ImageView) headerView.findViewById(R.id.img_advertisement);
        gridView.addHeaderView(headerView);

        Resources resources = getResources();
        website_name = resources.getString(R.string.website_name);

        new DataFetcherTask().execute();

        return rootView;
    }

    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            all_category = SyncDataFromServer.SyncAllCategory(website_name + "/all_products.txt");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            adapter = new ListViewAdapter(getActivity(), all_category.product_data);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                }
            });

            final android.os.Handler mHandler = new android.os.Handler();
            final Runnable mUpdateResults = new Runnable() {
                public void run() {
                    if (all_category.ads_data.size() > 0) {
                        AnimateandSlideShow();
                    }
                }
            };

            int delay = 500; // delay for 1 sec.
            int period = 1500; // repeat every 4 sec.
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    mHandler.post(mUpdateResults);
                }
            }, delay, period);

        }
    }

    private void AnimateandSlideShow() {
        imageLoader.DisplayImage(website_name + all_category.ads_data.get(currentimageindex %
                all_category.ads_data.size()).get("cover_photo"), slidingimage);
        currentimageindex++;
    }
}
