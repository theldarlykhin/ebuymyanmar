package com.hnttechs.www.ebuymyanmar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dell on 11/13/16.
 */
public class Detail extends AppCompatActivity implements View.OnClickListener {

    String product_id;
    String category;
    String website_name;

    HashMap<String, String> map = new HashMap<String, String>();
    ImageLoader imageLoader = new ImageLoader(getBaseContext());
    List<String> spinnerArray =  new ArrayList<String>();
    List<String> spinnerArray_delivery =  new ArrayList<String>();

    ImageView img_cloth;
    ImageView img_cloth1;
    ImageView img_cloth2;
    ImageView img_cloth3;
    ImageView img_cloth4;
    ImageView img_cloth5;
    ImageView btn_add_to_cart;
    ImageView btn_wish;
    ImageView btn_share;
    ImageView img_view_more4;
    ImageView img_related_product_1;
    ImageView img_related_product_2;

    TextView txt_title;
    TextView txt_youtube_link;
    TextView txt_actual_price;
    TextView txt_offer_price;
    TextView txt_shipping_from;
    TextView txt_delivery_rate;
    TextView txt_estimate_delivery;
    TextView btn_buy_now;
    TextView lbl_view_more;

    EditText txt_customer_review;

    Spinner spn_quantity;
    Spinner spn_delivery_service;
    Spinner spn_size;
    Spinner spn_color;

    private SharedPreferences mPreferences;
    Button btn_submit;

    FullLengthListView lv_qanda;
    FullLengthListView lv_customer_review;
    ListViewAdapter_qanda adapter_qanda;
    ListViewAdapter_customer_review adapter_customer_review;
    SyncDataFromServer.productDetailData aok;
    ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Product Detail");
        toolbar.setLogo(R.drawable.ebuy_menu_logo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        product_id = i.getStringExtra("product_id");
        category = i.getStringExtra("category");

        Resources resources = getResources();
        website_name = resources.getString(R.string.website_name);


        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);


        img_cloth = (ImageView)findViewById(R.id.img_cloth);
        img_cloth1 = (ImageView)findViewById(R.id.img_cloth1);
        img_cloth2 = (ImageView)findViewById(R.id.img_cloth2);
        img_cloth3 = (ImageView)findViewById(R.id.img_cloth3);
        img_cloth4 = (ImageView)findViewById(R.id.img_cloth4);
        img_cloth5 = (ImageView)findViewById(R.id.img_cloth5);
        img_related_product_1 = (ImageView)findViewById(R.id.related_product_1);
        img_related_product_2 = (ImageView)findViewById(R.id.related_product_2);
        txt_title = (TextView)findViewById(R.id.txt_title);
        txt_youtube_link = (TextView)findViewById(R.id.youtube_link);
        txt_actual_price = (TextView)findViewById(R.id.txt_actual_price);
        txt_offer_price = (TextView)findViewById(R.id.txt_offer_price);
        txt_shipping_from = (TextView)findViewById(R.id.txt_shipping_from);
        txt_delivery_rate = (TextView)findViewById(R.id.txt_delivery_rate);
        txt_estimate_delivery = (TextView)findViewById(R.id.txt_estimate_delivery);
        txt_customer_review = (EditText)findViewById(R.id.txt_customer_review);
        spn_delivery_service = (Spinner)findViewById(R.id.spn_delivery_service);
        spn_quantity = (Spinner)findViewById(R.id.spn_quantity);
        spn_size = (Spinner)findViewById(R.id.spn_size);
        spn_color = (Spinner)findViewById(R.id.spn_color);
        btn_buy_now = (TextView)findViewById(R.id.btn_buy_now);
        btn_add_to_cart = (ImageView)findViewById(R.id.btn_add_to_cart);
        btn_wish = (ImageView)findViewById(R.id.btn_wish);
        btn_share = (ImageView)findViewById(R.id.btn_share);
        lbl_view_more = (TextView)findViewById(R.id.lbl_view_more);
        img_view_more4 = (ImageView)findViewById(R.id.img_view_more4);
        lv_qanda = (FullLengthListView)findViewById(R.id.lv_qanda);
        lv_customer_review = (FullLengthListView)findViewById(R.id.lv_customer_review);
        btn_submit = (Button)findViewById(R.id.btn_submit);


        spinnerArray_delivery.add(0,"Pick Up");
        spinnerArray_delivery.add(1,"eBuy Delivery");

        Zawgyi_SpinnerAdapter adapter = new Zawgyi_SpinnerAdapter(getBaseContext(), android.R.layout.simple_spinner_item, spinnerArray_delivery);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_delivery_service.setAdapter(adapter);

        new DataFetcherTask().execute();

        img_cloth1.setOnClickListener(this);
        img_cloth2.setOnClickListener(this);
        img_cloth3.setOnClickListener(this);
        img_cloth4.setOnClickListener(this);
        img_cloth5.setOnClickListener(this);
        lbl_view_more.setOnClickListener(this);
        img_view_more4.setOnClickListener(this);
        btn_share.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        btn_buy_now.setOnClickListener(this);
        txt_youtube_link.setOnClickListener(this);
        btn_add_to_cart.setOnClickListener(this);

        lv_qanda.setFocusable(false);
        lv_customer_review.setFocusable(false);
    }

    public void onClick(View v) {
        switch(v.getId()) {
//            case R.id.img_cloth1:
//                imageLoader.DisplayImage(website_name + aok.map.get("avatar1"), img_cloth);
//                break;
//            case R.id.img_cloth2:
//                imageLoader.DisplayImage(website_name + aok.map.get("avatar2"), img_cloth);
//                break;
//            case R.id.img_cloth3:
//                imageLoader.DisplayImage(website_name + aok.map.get("avatar3"), img_cloth);
//                break;
//            case R.id.img_cloth4:
//                imageLoader.DisplayImage(website_name + aok.map.get("avatar4"), img_cloth);
//                break;
//            case R.id.img_cloth5:
//                imageLoader.DisplayImage(website_name + aok.map.get("avatar5"), img_cloth);
//                break;
            case R.id.img_view_more4:
//                Intent i = new Intent(getBaseContext(),ProductNoticePolicy.class);
//                i.putExtra("notice",aok.map.get("notice"));
//                startActivity(i);
//                break;
            case R.id.lbl_view_more:
//                Intent notice_intent = new Intent(getBaseContext(),ProductNoticePolicy.class);
//                notice_intent.putExtra("notice", aok.map.get("notice"));
//                startActivity(notice_intent);
//                break;
            case R.id.btn_buy_now:
                Intent order_form_intent = new Intent(getBaseContext(),ActivityOrderForm.class);
                order_form_intent .putExtra("product_id", product_id);
                order_form_intent.putExtra("category", category);
                order_form_intent.putExtra("title", aok.map.get("title"));
                order_form_intent.putExtra("price", aok.map.get("actual_pirce"));
                order_form_intent.putExtra("discount_price", aok.map.get("discount_price"));
//                order_form_intent.putExtra("avatar1", aok.map.get("avatar1"));
                if(!aok.map.get("product_size").equals("")) {
                    order_form_intent.putExtra("size", spn_size.getSelectedItem().toString());
                } else {
                    order_form_intent.putExtra("size", "");
                }
                if(!aok.map.get("product_color").equals("")) {
                    order_form_intent.putExtra("color",spn_color.getSelectedItem().toString());
                } else {
                    order_form_intent.putExtra("color","");
                }
                order_form_intent.putExtra("qty",spn_quantity.getSelectedItem().toString());
                startActivity(order_form_intent );
                break;
            case R.id.btn_add_to_cart:
                if (mPreferences.contains("BuyerId")) {
                    dialog = ProgressDialog.show(Detail.this, "",
                            "Sending...", true);
                    dialog.show();
                    new AddToCart().execute(website_name + "/add_to_cart_mobi", product_id, aok.map.get("product_color"),
                            aok.map.get("product_size"), spn_delivery_service.getSelectedItem().toString(),
                            spn_quantity.getSelectedItem().toString(), aok.map.get("title"),
                            mPreferences.getString("BuyerId", ""));
                }else {
                    dialog = ProgressDialog.show(Detail.this, "",
                            "Sending...", true);
                    dialog.show();
                    if (mPreferences.contains("cart_key")) {
                        new AddToCart_with_key().execute(website_name + "/add_to_cart_mobi_with_key",
                                product_id, aok.map.get("product_color"),
                                aok.map.get("product_size"), spn_delivery_service.getSelectedItem().toString(),
                                spn_quantity.getSelectedItem().toString(), aok.map.get("title"),
                                mPreferences.getString("cart_key", ""));
                    } else {
                        new AddToCart_with_key().execute(website_name + "/add_to_cart_mobi_with_key", product_id, aok.map.get("product_color"),
                                aok.map.get("product_size"), spn_delivery_service.getSelectedItem().toString(),
                                spn_quantity.getSelectedItem().toString(), aok.map.get("title"), "");
                    }
                }
                break;
            case R.id.btn_share:
                shareIt(website_name + "/product_by_id.txt?id=" + product_id + "&category=" + category);
                break;
            case R.id.youtube_link:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=Hxy8BZGQ5Jo")));
                Log.i("Video", "Video Playing....");
                break;
            case R.id.btn_submit:
                dialog = ProgressDialog.show(Detail.this, "",
                        "Sending...", true);
                dialog.show();
                new MyAsyncTask().execute(txt_title.getText().toString(),
                        txt_customer_review.getText().toString(),product_id,"htoomyat",website_name);
                break;
        }
    }

    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            aok = SyncDataFromServer.ProductDetail(website_name +
                    "/product_by_id.txt?id=" + product_id + "&category=" + category);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

//            imageLoader.DisplayImage(website_name + aok.map.get("avatar1"), img_cloth);
//            imageLoader.DisplayImage(website_name + aok.map.get("avatar1"), img_cloth1);
//            imageLoader.DisplayImage(website_name + aok.map.get("avatar2"), img_cloth2);
//            imageLoader.DisplayImage(website_name + aok.map.get("avatar3"), img_cloth3);
//            imageLoader.DisplayImage(website_name + aok.map.get("avatar4"), img_cloth4);
//            imageLoader.DisplayImage(website_name + aok.map.get("avatar5"), img_cloth5);

            txt_title.setText(aok.map.get("title"));
            txt_actual_price.setText(aok.map.get("actual_pirce"));
            txt_offer_price.setText(aok.map.get("discount_price"));
            txt_delivery_rate.setText(aok.map.get("delivery_rate"));
            txt_estimate_delivery.setText(aok.map.get("delivery_time"));

//int cok = Integer.parseInt(aok.map.get("quantity"));
//            int dok;
//            for(int i=0;i<=cok;i++)
//            {
//                dok = i;


//            Integer[] items = new Integer[]{1,2,3,4};
//            Zawgyi_SpinnerAdapter adapter = new Zawgyi_SpinnerAdapter(this, android.R.layout.simple_spinner_item, items);
//            spn_quantity.setAdapter(adapter);

//            }


            int cok = Integer.parseInt(aok.map.get("quantity"));
            String dok;
            for(int i=1;i<=cok;i++) {
                dok = ""+i+"";
                spinnerArray.add(dok);
            }
            Zawgyi_SpinnerAdapter adapter = new Zawgyi_SpinnerAdapter(getBaseContext(), android.R.layout.simple_spinner_item, spinnerArray);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spn_quantity.setAdapter(adapter);

            if(!aok.map.get("product_size").equals("")) {
                Additional_Method.fill_data_to_spinner_with_arraylist(getBaseContext(),
                        aok.map.get("product_size"), spn_size);
            }
            if(!aok.map.get("product_color").equals("")) {
                Additional_Method.fill_data_to_spinner_with_arraylist(getBaseContext(),
                        aok.map.get("product_color"), spn_color);
            }
//            adapter_qanda = new ListViewAdapter_qanda(getBaseContext(), aok.qanda);
//            lv_qanda.setAdapter(adapter_qanda);
//
//            adapter_customer_review = new ListViewAdapter_customer_review(getBaseContext(), aok.customer_review);
//            lv_customer_review.setAdapter(adapter_customer_review);

//            imageLoader.DisplayImage(website_name + aok.related_products.get(0).get("avatar1"),
//                    img_related_product_1);
        }
    }

    private void shareIt(String url) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Share");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, url);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, Double> {

        @Override
        protected Double doInBackground(String... params) {
            SendDataToServer.postReview(params[0], params[1], params[2], params[3], params[4]);
            return null;
        }

        protected void onPostExecute(Double result) {
            txt_customer_review.setText("");
            Toast.makeText(getApplicationContext(), "command sent", Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }

        protected void onProgressUpdate(Integer... progress) {
        }
    }

    private class AddToCart extends AsyncTask<String, Integer, Double> {

        @Override
        protected Double doInBackground(String... params) {
            SendDataToServer.add_to_cart(params[0], params[1], params[2], params[3], params[4],
                    params[5], params[6], params[7]);
            return null;
        }

        protected void onPostExecute(Double result) {
            Toast.makeText(getApplicationContext(), "command sent", Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }

        protected void onProgressUpdate(Integer... progress) {
        }
    }


    private class AddToCart_with_key extends AsyncTask<String, Integer, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            String response = null;
            JSONObject json = new JSONObject();
            try {
                response = SendDataToServer.add_to_cart_with_key(params[0], params[1], params[2], params[3], params[4],
                    params[5], params[6], params[7]);
                json = new JSONObject(response);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("JSON", "" + e);
            }

            return json;
        }

        protected void onPostExecute(JSONObject json) {
            try {
                if (json.getBoolean("success")) {
                    SharedPreferences.Editor editor = mPreferences.edit();
                    editor.putString("cart_key", json.getString("key"));
                    editor.commit();

                    Toast.makeText(getApplicationContext(), "command sent with key " + json.getString("key"),
                            Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } else {
                    Toast.makeText(getApplicationContext(), "command Fail", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            } catch (Exception e) {
                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        protected void onProgressUpdate(Integer... progress) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }
}

