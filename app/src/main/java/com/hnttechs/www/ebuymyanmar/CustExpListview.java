package com.hnttechs.www.ebuymyanmar;

import android.content.Context;
import android.widget.ExpandableListView;

/**
 * Created by dell on 11/23/16.
 */
public class CustExpListview extends ExpandableListView {

    int intGroupPosition, intChildPosition, intGroupid;

    public CustExpListview(Context context)
    {
        super(context);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(960, MeasureSpec.AT_MOST);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(5000, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
