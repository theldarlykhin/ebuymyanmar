package com.hnttechs.www.ebuymyanmar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/4/15.
 */
public class ListViewAdapter_customer_review extends BaseAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> grid_data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ImageLoader imageLoader = new ImageLoader(context);

    public ListViewAdapter_customer_review(Context context, ArrayList<HashMap<String, String>> listData) {
        this.context = context;
        grid_data = listData;

    }

    @Override
    public int getCount() {
        return grid_data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.lv_item_customer_review, null);
            resultp = grid_data.get(position);

            TextView buyer_name = (TextView) gridView.findViewById(R.id.buyer_name);
            TextView buyer_comment = (TextView) gridView.findViewById(R.id.buyer_comment);

            buyer_name.setText(resultp.get("buyer_name"));
            buyer_comment.setText(resultp.get("buyer_comment"));

        } else {
            gridView = (View) convertView;
        }
        return gridView;
    }
}