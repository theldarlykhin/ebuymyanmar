package com.hnttechs.www.ebuymyanmar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/4/15.
 */
public class ListViewAdapter_cart extends BaseAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> grid_data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ImageLoader imageLoader = new ImageLoader(context);

    public ListViewAdapter_cart(Context context, ArrayList<HashMap<String, String>> listData) {
        this.context = context;
        grid_data = listData;

    }

    @Override
    public int getCount() {
        return grid_data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.cart_list_item, null);

            resultp = grid_data.get(position);

            TextView title = (TextView) gridView.findViewById(R.id.lbl_title);
            TextView model_no = (TextView) gridView.findViewById(R.id.lbl_model_no);
            TextView price = (TextView) gridView.findViewById(R.id.lbl_price);
            ImageView img_cloth = (ImageView) gridView.findViewById(R.id.img_cloth);
            TextView quantity = (TextView) gridView.findViewById(R.id.lbl_qty);
            TextView size = (TextView) gridView.findViewById(R.id.lbl_size);
            TextView color= (TextView) gridView.findViewById(R.id.lbl_color);
            Button btn_remove =(Button)gridView.findViewById(R.id.btn_remove);

            title.setText(resultp.get("title"));
            model_no.setText(resultp.get("product_model"));
            price.setText("20000 Kyat");
            quantity.setText(resultp.get("quantity"));
            size.setText(resultp.get("product_size"));
            color.setText(resultp.get("product_color"));

            btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        } else {
            gridView = (View) convertView;
        }
        return gridView;
    }
}