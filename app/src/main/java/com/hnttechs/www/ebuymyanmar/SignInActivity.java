package com.hnttechs.www.ebuymyanmar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by dell on 10/4/16.
 */
public class SignInActivity extends Activity {


    private SharedPreferences mPreferences;
    RelativeLayout relativelayout;
    static RelativeLayout layout_logo;
    RelativeLayout username_stroke;
    RelativeLayout password_stroke;
    ImageView img_icon;
    ImageView username_icon;
    ImageView password_icon;
    EditText txt_username;
    EditText txt_password;
    RelativeLayout.LayoutParams relativeLayoutParams;
    RelativeLayout.LayoutParams buttonlayoutparams;
    Drawable mDrawable_username_icon_red;
    Drawable mDrawable_username_icon_gray;
    Drawable mDrawable_password_icon_red;
    Drawable mDrawable_password_icon_gray;
    Button btn_sign_in;
    Button btn_sign_up;
    private String m_email;
    private String m_password;
    static int layout_log_height;
    String website_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


        relativelayout = (RelativeLayout)findViewById(R.id.relativeLayout);
        layout_logo = (RelativeLayout)findViewById(R.id.layout_logo);
        username_stroke = (RelativeLayout)findViewById(R.id.username_stroke);
        password_stroke = (RelativeLayout)findViewById(R.id.password_stroke);
        img_icon = (ImageView)findViewById(R.id.img_icon);
        username_icon = (ImageView)findViewById(R.id.username_icon);
        password_icon = (ImageView)findViewById(R.id.password_icon);
        txt_username = (EditText)findViewById(R.id.txt_username);
        txt_password = (EditText)findViewById(R.id.txt_password);
        btn_sign_in = (Button)findViewById(R.id.btn_sign_in);
        btn_sign_up = (Button)findViewById(R.id.btn_sign_up);


        relativeLayoutParams = new RelativeLayout.LayoutParams
                (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        relativeLayoutParams.height = height/4*4;
        relativeLayoutParams.setMargins(width / 20, 120 , width / 20, height / 5);
        relativelayout.setLayoutParams(relativeLayoutParams);

        mDrawable_password_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
        mDrawable_password_icon_gray.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
        mDrawable_username_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
        mDrawable_username_icon_gray.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


        username_icon.setImageDrawable(mDrawable_username_icon_gray);
        password_icon.setImageDrawable(mDrawable_password_icon_gray);

        username_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
        password_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

        Resources resources = getResources();
        website_name = resources.getString(R.string.website_name);

        txt_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                mDrawable_username_icon_red = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
                mDrawable_username_icon_red.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));
                username_icon.setImageDrawable(mDrawable_username_icon_red);

                username_stroke.setBackgroundColor(Color.parseColor("#ee1f25"));

                mDrawable_password_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
                mDrawable_password_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                password_icon.setImageDrawable(mDrawable_password_icon_gray);

                password_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
            }
        });


        txt_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                mDrawable_password_icon_red = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
                mDrawable_password_icon_red.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));
                password_icon.setImageDrawable(mDrawable_password_icon_red);

                password_stroke.setBackgroundColor(Color.parseColor("#ee1f25"));


                mDrawable_username_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
                mDrawable_username_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                username_icon.setImageDrawable(mDrawable_username_icon_gray);

                username_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
            }
        });


        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_email = txt_username.getText().toString();
                m_password = txt_password.getText().toString();

                LoginTask loginTask = new LoginTask(SignInActivity.this);
                loginTask.setMessageLoading("Logging in...");
                loginTask.execute(website_name+"/myapi/v1/sessions");
            }
        });


        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sign_up_intent = new Intent(getBaseContext(),SignUpActivity.class);
                startActivity(sign_up_intent);
                finish();
            }
        });

    }

    private class LoginTask extends UrlJsonAsyncTask {
        public LoginTask(Context context) {
            super(context);
        }

        @Override
        protected JSONObject doInBackground(String... urls) {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urls[0]);
            JSONObject holder = new JSONObject();
            JSONObject userObj = new JSONObject();
            String response = null;
            JSONObject json = new JSONObject();

            try {
                try {
                    // setup the returned values in case
                    // something goes wrong
                    json.put("success", false);
                    json.put("info", "Something went wrong. Retry!");
                    // add the user email and password to
                    // the params
                    userObj.put("email", m_email);
                    userObj.put("password", m_password);
                    holder.put("buyer",userObj);
                    StringEntity se = new StringEntity(holder.toString());
                    post.setEntity(se);

                    // setup the request headers
                    post.setHeader("Accept", "application/json");
                    post.setHeader("Content-Type", "application/json");

                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    response = client.execute(post, responseHandler);
                    json = new JSONObject(response);

                } catch (HttpResponseException e) {
                    e.printStackTrace();
                    Log.e("ClientProtocol", "" + e);
                    json.put("info", "Email and/or password are invalid. Retry!");
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("IO", "" + e);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("JSON", "" + e);
            }

            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                if (json.getBoolean("success")) {
                    SharedPreferences.Editor editor = mPreferences.edit();
                    editor.putString("BuyerId", json.getJSONObject("data").getString("buyer_id"));
                    editor.putString("BuyerName", json.getJSONObject("data").getString("buyer_name"));
                    editor.putString("Buyeremail", json.getJSONObject("data").getString("buyer_email"));
                    editor.commit();

                    Intent intent = new Intent(getApplicationContext(), DrawerActivity.class);
                    startActivity(intent);
                    finish();
                }
                Toast.makeText(context, json.getString("info"), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            } finally {
                super.onPostExecute(json);
            }
        }
    }

}
