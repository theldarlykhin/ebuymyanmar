package com.hnttechs.www.ebuymyanmar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by dell on 4/26/16.
 */
public class Chat extends ActionBarActivity {

    final String MESSAGES_ENDPOINT = "http://www.myanmardealer.com/send_to_pusher?";

    MessageAdapter messageAdapter;
    EditText messageInput;
    Button sendButton;
    static String username, seller_name;
    String userId, sellerId;
    String my_message;
    static ListView messagesView;
    static Pusher pusher_me;
    static Channel channel_send;
    static Channel channel_me;
    static SharedPreferences mPreferences;
    static int status = 0;
    static int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        seller_name = i.getStringExtra("seller_name");
        sellerId = i.getStringExtra("seller_id");

        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        username = mPreferences.getString("UserName", "0");
        userId = mPreferences.getString("UserId", "0");

        messageInput = (EditText) findViewById(R.id.message_input);
        messagesView = (ListView) findViewById(R.id.messages_view);

        messageAdapter = new MessageAdapter(this, new ArrayList<Message>());
        messagesView.setAdapter(messageAdapter);
        count =0;

        pusher_me = new Pusher("b218d631b3532773d67f");
        pusher_me.connect();

        channel_send = pusher_me.subscribe(username + seller_name);
        channel_me = pusher_me.subscribe(seller_name + username);
        sendButton = (Button) findViewById(R.id.send_button);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postMessage();
            }
        });


        channel_send.bind("my_event", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Gson gson = new Gson();
                        Message message = gson.fromJson(data, Message.class);
                        messageAdapter.add(message);
                        messagesView.setSelection(messageAdapter.getCount() - 1);
                    }

                });
            }

        });

        channel_me.bind("my_event", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Gson gson = new Gson();
                        Message message = gson.fromJson(data, Message.class);
                        messageAdapter.add(message);
                        messagesView.setSelection(messageAdapter.getCount() - 1);
                    }

                });
            }

        });

    }

//    @Override
//    public boolean onKey(View v, int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP){
//            postMessage();
//        }
//        return true;
//    }


    private void postMessage() {
        String text = messageInput.getText().toString();

        if (text.equals("")) {
            return;
        }

        RequestParams params = new RequestParams();

        params.put("message", text);
        params.put("channels", username + seller_name);
        params.put("recipient_id", sellerId);
        params.put("sender_id", userId);

        my_message = messageInput.getText().toString();


        AsyncHttpClient client = new AsyncHttpClient();

        client.get(MESSAGES_ENDPOINT + params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        messageInput.setText("");
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        messageInput.setText("");
                    }
                });
            }
        });

    }
}

