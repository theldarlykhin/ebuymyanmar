package com.hnttechs.www.ebuymyanmar;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by dell on 10/29/16.
 */
public class TestActivity extends ActionBarActivity {

    static TextView txt_change_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);

        LocaleHelper.onCreate(this, "mm");

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.menu));
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));


        Typeface font = Typeface.createFromAsset(getBaseContext().getAssets(),
                "fonts/zawgyione.ttf");


        txt_change_text = (TextView)findViewById(R.id.txt_change_text);

        txt_change_text.setTypeface(font);

        Resources resources = getResources();
        txt_change_text.setText(resources.getString(R.string.hello_world));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.myanmar:
                LocaleHelper.setLocale(this, "mm");
                updateViews();
                return true;
            case R.id.america:
                LocaleHelper.setLocale(this, "en");
                updateViews();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateViews() {
        // if you want you just call activity to restart itself to redraw all the widgets with the correct locale
        // however, it will cause a bad look and feel for your users
        //
        // this.recreate();

        //or you can just update the visible text on your current layout
        Resources resources = getResources();

        txt_change_text.setText(resources.getString(R.string.hello_world));
    }


}
