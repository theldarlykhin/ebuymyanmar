package com.hnttechs.www.ebuymyanmar;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 5/7/16.
 */
public class WomensFashion extends Fragment {
    GridView gridView;

    static ArrayList<HashMap<String, String>> arraylist;
    ListViewAdapter adapter;
    String website_name;
    String category_name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_authentic_brand_zone, container, false);

        gridView = (GridView) rootView.findViewById(R.id.gridView1);

        Resources resources = getResources();
        website_name = resources.getString(R.string.website_name);
        category_name = "WomensFashion";

        new DataFetcherTask().execute();

        return rootView;
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            arraylist = SyncDataFromServer.SyncData(website_name +
                    "/product_by_category.txt?category=" + category_name);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            adapter = new ListViewAdapter(getActivity(), arraylist);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getActivity(),Detail.class);
                    i.putExtra("product_id", arraylist.get(position).get("id"));
                    i.putExtra("category", category_name);
                    startActivity(i);
                }
            });
        }
    }
}
