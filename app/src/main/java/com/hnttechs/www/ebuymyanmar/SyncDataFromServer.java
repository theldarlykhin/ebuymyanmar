package com.hnttechs.www.ebuymyanmar;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 12/10/16.
 */
public class SyncDataFromServer {
    static String serverData;
    static ArrayList<HashMap<String, String>> arraylist;

    public static ArrayList<HashMap<String, String>> SyncData(String url) {
        serverData = null;

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            serverData = EntityUtils.toString(httpEntity);
            Log.d("response", serverData);

            JSONObject jsonObject = new JSONObject(serverData);
            JSONArray jsonArray = jsonObject.getJSONArray("product");
            arraylist = new ArrayList<HashMap<String, String>>();

            for (int i = 0; i < jsonArray.length(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                JSONObject jsonObjectClothing = jsonArray.getJSONObject(i);

                map.put("id", jsonObjectClothing.getString("id"));
                map.put("short_title", jsonObjectClothing.getString("short_title"));
                map.put("discount_price", jsonObjectClothing.getString("discount_price"));
                map.put("avatar1", jsonObjectClothing.getString("avatar1"));

                arraylist.add(map);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arraylist;
    }


    public static HashMap<String, String> SelectMyInfo(String url) {
        String serverMyInfo = null;
        HashMap<String, String> map = new HashMap<String, String>();

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            serverMyInfo = EntityUtils.toString(httpEntity);
            Log.d("response", serverMyInfo);

            JSONArray jsonArray = new JSONArray(serverMyInfo);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            map.put("id", jsonObject.getString("id"));
            map.put("buyer_name", jsonObject.getString("buyer_name"));
            map.put("buyer_phone", jsonObject.getString("buyer_phone"));
            map.put("buyer_township", jsonObject.getString("buyer_township"));
            map.put("buyer_address", jsonObject.getString("buyer_address"));

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return map;
    }


    public static ArrayList<HashMap<String, String>> GetCart(String url) {

        String serverMyInfo = null;
        HashMap<String, String> map = new HashMap<String, String>();

        ArrayList<HashMap<String, String>> Cart_arrayList = new ArrayList<HashMap<String, String>>();
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            serverMyInfo = EntityUtils.toString(httpEntity);
            Log.d("response", serverMyInfo);


            JSONObject jsonObject = new JSONObject(serverMyInfo);
            JSONArray jsonArray = jsonObject.getJSONArray("cart");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjectClothing = jsonArray.getJSONObject(i);

                map.put("product_id", jsonObjectClothing.getString("product_id"));
                map.put("cart_id", jsonObjectClothing.getString("cart_id"));
                map.put("quantity", jsonObjectClothing.getString("quantity"));
                map.put("product_size", jsonObjectClothing.getString("product_size"));
                map.put("product_color", jsonObjectClothing.getString("product_color"));
                map.put("delivery_method", jsonObjectClothing.getString("delivery_method"));
                map.put("product_model", jsonObjectClothing.getString("product_model"));
//                map.put("price", jsonObjectClothing.getString("price"));

                Cart_arrayList.add(map);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Cart_arrayList;
    }

    public static class productDetailData {
        public HashMap<String, String> map;
        public ArrayList<HashMap<String, String>> qanda;
        public ArrayList<HashMap<String, String>> customer_review;
        public ArrayList<HashMap<String, String>> related_products;
    }

    public static productDetailData ProductDetail(String url) {
        serverData = null;
        productDetailData all_data = new productDetailData();

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        HashMap<String, String> map = new HashMap<String, String>();
        ArrayList<HashMap<String, String>> arraylist_qanda = new ArrayList<HashMap<String, String>>();
        ArrayList<HashMap<String, String>> arraylist_customer_review = new ArrayList<HashMap<String, String>>();
        ArrayList<HashMap<String, String>> arraylist_related_product = new ArrayList<HashMap<String, String>>();
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            serverData = EntityUtils.toString(httpEntity);
            Log.d("response", serverData);

            JSONObject jsonObject = new JSONObject(serverData);
            JSONArray jsonArray = jsonObject.getJSONArray("product");
//            JSONArray jsonArray_related_product = jsonObject.getJSONArray("related_product");
//            JSONArray jsonArray_customer_review = jsonObject.getJSONArray("customer_review");
            JSONObject jsonObjectClothing = jsonArray.getJSONObject(0);

            map.put("title", jsonObjectClothing.getString("title"));
            map.put("actual_pirce", jsonObjectClothing.getString("actual_price"));
            map.put("discount_price", jsonObjectClothing.getString("discount_price"));
            map.put("quantity", jsonObjectClothing.getString("quantity"));
//            map.put("specsq", jsonObjectClothing.getString("specsq"));
//            map.put("specsa", jsonObjectClothing.getString("specsa"));
            map.put("product_category", jsonObjectClothing.getString("product_category"));
            map.put("pick_up", jsonObjectClothing.getString("pick_up"));
            map.put("ebuy_delivery", jsonObjectClothing.getString("ebuy_delivery"));
            map.put("delivery_time", jsonObjectClothing.getString("delivery_time"));
            map.put("product_video", jsonObjectClothing.getString("product_video"));
            map.put("product_size", jsonObjectClothing.getString("product_size"));
            map.put("product_color", jsonObjectClothing.getString("product_color"));
            map.put("delivery_rate", jsonObjectClothing.getString("delivery_rate"));
//            map.put("question", jsonObjectClothing.getString("question"));
//            map.put("answer", jsonObjectClothing.getString("answer"));
//            map.put("notice", jsonObjectClothing.getString("notice"));
//            map.put("description", jsonObjectClothing.getString("description"));
//            map.put("avatar1", jsonObjectClothing.getString("avatar1"));
//            map.put("avatar2", jsonObjectClothing.getString("avatar2"));
//            map.put("avatar3", jsonObjectClothing.getString("avatar3"));
//            map.put("avatar4", jsonObjectClothing.getString("avatar4"));
//            map.put("avatar5", jsonObjectClothing.getString("avatar5"));

//            JSONArray jsonArray_question = jsonObjectClothing.getJSONArray("question");
//            JSONArray jsonArray_answer = jsonObjectClothing.getJSONArray("answer");
//
//            for (int i = 0; i < jsonArray_question.length(); i++) {
//                HashMap<String, String> map_qanda = new HashMap<String, String>();
//                map_qanda.put("question", jsonArray_question.get(i).toString());
//                map_qanda.put("answer", jsonArray_answer.get(i).toString());
//
//                arraylist_qanda.add(map_qanda);
//            }
//
//            for (int k = 0; k < jsonArray_customer_review.length(); k++) {
//
//                HashMap<String, String> map_customer_review= new HashMap<String, String>();
//                JSONObject jsonObjectClothing_customer_review = jsonArray_customer_review.getJSONObject(k);
//
//                map_customer_review.put("buyer_comment", jsonObjectClothing_customer_review.getString("buyer_comment"));
//                map_customer_review.put("buyer_name", jsonObjectClothing_customer_review.getString("buyer_name"));
//
//                arraylist_customer_review.add(map_customer_review);
//            }

//            for (int j = 0; j < jsonArray_related_product.length(); j++) {
//
//                HashMap<String, String> map_related_product = new HashMap<String, String>();
//                JSONObject jsonObjectClothing_related_product = jsonArray_related_product.getJSONObject(j);
//
//                map_related_product.put("id", jsonObjectClothing_related_product.getString("id"));
//                map_related_product.put("short_title", jsonObjectClothing_related_product.getString("short_title"));
//                map_related_product.put("discount_price", jsonObjectClothing_related_product.getString("discount_price"));
//                map_related_product.put("avatar1", jsonObjectClothing_related_product.getString("avatar1"));
//
//                arraylist_related_product.add(map_related_product);
//            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        all_data.map = map;
//        all_data.qanda = arraylist_qanda;
        all_data.related_products = arraylist_related_product;
        all_data.customer_review = arraylist_customer_review;
        return all_data;
    }


    public static class allcategory {
        public ArrayList<HashMap<String, String>> product_data;
        public ArrayList<HashMap<String, String>> ads_data;
    }


    public static allcategory SyncAllCategory(String url) {
        serverData = null;
        allcategory allcategory = new allcategory();
        ArrayList<HashMap<String, String>> arraylist = new ArrayList<HashMap<String, String>>();
        ArrayList<HashMap<String, String>> arraylist_advertisement = new ArrayList<HashMap<String, String>>();

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            serverData = EntityUtils.toString(httpEntity);
            Log.d("response", serverData);

            JSONObject jsonObject = new JSONObject(serverData);
            JSONArray jsonArray = jsonObject.getJSONArray("product");
            JSONArray jsonArray_advertisement = jsonObject.getJSONArray("advertisement");

            for (int j = 0; j < jsonArray.length(); j++) {
                HashMap<String, String> map_p = new HashMap<String, String>();
                JSONObject jsonObjectClothing_p = jsonArray.getJSONObject(j);

                map_p.put("id", jsonObjectClothing_p.getString("id"));
                map_p.put("short_title", jsonObjectClothing_p.getString("short_title"));
                map_p.put("discount_price", jsonObjectClothing_p.getString("discount_price"));
                map_p.put("avatar1", jsonObjectClothing_p.getString("avatar1"));

                arraylist.add(map_p);
            }

            for (int i = 0; i < jsonArray_advertisement.length(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                JSONObject jsonObjectClothing = jsonArray_advertisement.getJSONObject(i);

                map.put("id", jsonObjectClothing.getString("id"));
                map.put("cover_photo", jsonObjectClothing.getString("cover_photo"));

                arraylist_advertisement.add(map);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        allcategory.product_data = arraylist;
        allcategory.ads_data = arraylist_advertisement;
        return allcategory;
    }
}
