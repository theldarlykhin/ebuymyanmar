package com.hnttechs.www.ebuymyanmar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by dell on 12/30/16.
 */
public class ActivityOrderForm extends AppCompatActivity implements View.OnClickListener {

    String product_id;
    String category;
    String title;
    String price;
    String discount_price;
    String avatar1;
    String size;
    String str_color;
    String qty;

    TextView lbl_title;
    TextView lbl_model_no;
    TextView lbl_qty;
    TextView lbl_size;
    TextView lbl_color;
    TextView lbl_price;

    EditText additional_msg;
    EditText name;
    EditText email;
    EditText phone_no;
    EditText address;
    EditText city;
    EditText township;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_form);


        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Order Form");
        toolbar.setLogo(R.drawable.ebuy_menu_logo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent i = getIntent();
        product_id = i.getStringExtra("product_id");
        category = i.getStringExtra("category");
        title = i.getStringExtra("title");
        price = i.getStringExtra("actual_pirce");
        discount_price = i.getStringExtra("discount_price");
//        avatar1 = i.getStringExtra("avatar1");
        size = i.getStringExtra("size");
        str_color = i.getStringExtra("color");
        qty = i.getStringExtra("qty");


        lbl_title = (TextView)findViewById(R.id.lbl_title);
        lbl_model_no = (TextView)findViewById(R.id.lbl_model_no);
        lbl_qty = (TextView)findViewById(R.id.lbl_qty);
        lbl_size = (TextView)findViewById(R.id.lbl_size);
        lbl_color = (TextView)findViewById(R.id.lbl_color);
        lbl_price = (TextView)findViewById(R.id.txt_total_price);

        additional_msg = (EditText)findViewById(R.id.txt_additional_message);
        name = (EditText)findViewById(R.id.edt_name);
        email = (EditText)findViewById(R.id.edt_email);
        phone_no = (EditText)findViewById(R.id.edt_phone);
        address = (EditText)findViewById(R.id.edt_address);
        city = (EditText)findViewById(R.id.edt_city);
        township = (EditText)findViewById(R.id.edt_township);

        Button btn_get_coupon_code = (Button)findViewById(R.id.btn_get_coupon_code);
        Button btn_submit = (Button)findViewById(R.id.btn_submit_order);
//        Button btn_edit = (Button)findViewById(R.id.btn_edit);

        lbl_title.setText(title);
        lbl_model_no.setText("");
        lbl_qty.setText(qty);
        lbl_size.setText(size);
        lbl_color.setText(str_color);
        lbl_price.setText(discount_price);

        btn_get_coupon_code.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
//        btn_edit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_get_coupon_code:
                Intent coupon_intent = new Intent(getBaseContext(), ActivityCoupon.class);
                startActivity(coupon_intent);
                break;
            case R.id.btn_submit:
                dialog = ProgressDialog.show(ActivityOrderForm.this, "",
                        "Sending...", true);
                dialog.show();
//                new MyAsyncTask().execute(txt_title.getText().toString(),
//                        txt_customer_review.getText().toString(),product_id,"htoomyat",website_name);
                break;
//            case R.id.btn_edit:
//
//                break;
        }
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, Double> {

        @Override
        protected Double doInBackground(String... params) {
            SendDataToServer.postReview(params[0], params[1], params[2], params[3], params[4]);
            return null;
        }

        protected void onPostExecute(Double result) {
            Toast.makeText(getApplicationContext(), "command sent", Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }

        protected void onProgressUpdate(Integer... progress) {
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
