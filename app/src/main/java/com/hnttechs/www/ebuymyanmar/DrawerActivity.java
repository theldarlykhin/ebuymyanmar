package com.hnttechs.www.ebuymyanmar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by dell on 9/26/16.
 */
public class DrawerActivity extends ActionBarActivity {

    private ExpandableListView mDrawerList;
    private RelativeLayout menu_slide;
    private ActionBarDrawerToggle mDrawerToggle;
    private android.support.v4.widget.DrawerLayout mDrawerLayout;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private ExpandableListAdapter adapter;
    static String locale_name;
    static SharedPreferences mPreferences;

    private int lastExpandedPosition = -1;
    public static TextView txt_sign_in;
    public static TextView txt_sign_up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Home");
        toolbar.setLogo(R.drawable.ebuy_menu_logo);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.menu));
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        setSupportActionBar(toolbar);

        Locale locale = new Locale("en");
        Locale.setDefault(locale);
        updateViews();


        if (savedInstanceState == null) {
            displayView(0);
        }
    }
    private void updateViews() {
        Resources resources = getResources();
        mTitle = mDrawerTitle = getTitle();

        navMenuTitles = resources.getStringArray(R.array.nav_drawer_items);

        navMenuIcons = resources
                .obtainTypedArray(R.array.nav_drawer_icon);
        mDrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ExpandableListView) findViewById(R.id.list_slidermenu);
        menu_slide = (RelativeLayout) findViewById(R.id.menu_slide);

        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);


        navDrawerItems = new ArrayList<NavDrawerItem>();
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], navMenuIcons.getResourceId(9,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[10], navMenuIcons.getResourceId(10,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[11], navMenuIcons.getResourceId(11,-1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[12], navMenuIcons.getResourceId(12,-1)));
        if (mPreferences.contains("BuyerId")) {
            navDrawerItems.add(new NavDrawerItem("Sign out", navMenuIcons.getResourceId(12,-1)));
        }else {

            navDrawerItems.add(new NavDrawerItem("Sign in", navMenuIcons.getResourceId(12,-1)));
        }

        adapter = new ExpandableListAdapter(this, navDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    mDrawerList.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.logo,
                R.string.app_name,
                R.string.app_name
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        txt_sign_in = (TextView)findViewById(R.id.txt_sign_in);
        txt_sign_up = (TextView)findViewById(R.id.txt_sign_up);

        if (mPreferences.contains("BuyerId")) {
            txt_sign_in.setText("Sign Out");
            txt_sign_up.setVisibility(View.INVISIBLE);
        }else {
            txt_sign_in.setText("Sign In");
            txt_sign_up.setVisibility(View.VISIBLE);
        }

        txt_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txt_sign_in.getText().toString()=="Sign In") {
                    Intent intent_sign_in = new Intent(getBaseContext(), SignInActivity.class);
                    startActivity(intent_sign_in);
                }else if(txt_sign_in.getText().toString()=="Sign Out") {

                    mPreferences.edit().clear().commit();
                    Toast.makeText(getBaseContext(),"Logging Out", Toast.LENGTH_SHORT).show();
                    txt_sign_in.setText("Sign In");
                    txt_sign_up.setVisibility(View.VISIBLE);
                }
            }
        });

        txt_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_sign_up = new Intent(getBaseContext(),SignUpActivity.class);
                startActivity(intent_sign_up);
            }
        });

    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            displayView(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.myanmar:
                LocaleHelper.setLocale(this, "mm");
                locale_name = "mm";

                Locale myLocale = new Locale(locale_name);
                Resources res = getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = myLocale;
                res.updateConfiguration(conf, dm);
                Intent refresh = new Intent(this, DrawerActivity.class);
                startActivity(refresh);
                finish();

                return true;
            case R.id.america:
                LocaleHelper.setLocale(this, "en");
                locale_name = "en";

                Locale myLocale_en = new Locale(locale_name);
                Resources res_en = getResources();
                DisplayMetrics dm_en = res_en.getDisplayMetrics();
                Configuration conf_en = res_en.getConfiguration();
                conf_en.locale = myLocale_en;
                res_en.updateConfiguration(conf_en, dm_en);
                Intent refresh_en = new Intent(this, DrawerActivity.class);
                startActivity(refresh_en);
                finish();

                return true;
            case R.id.shopping_cart:
                Intent intent_cart = new Intent(getBaseContext(),ActivityCart.class);
                startActivity(intent_cart);
                return true;
            case R.id.menu_about_us:
                Intent intent_about_us = new Intent(getBaseContext(),ActivityAboutUs.class);
                startActivity(intent_about_us);
                return true;
            case R.id.menu_contact_us:
                Intent intent_contact_us = new Intent(getBaseContext(),ActivityContactUs.class);
                startActivity(intent_contact_us);
                return true;
            case R.id.menu_terms_and_conditions:
                Intent intent_tc = new Intent(getBaseContext(),ActivityTermsAndCondition.class);
                startActivity(intent_tc);
                return true;
            case R.id.menu_help_topics:
                Intent intent_hp = new Intent(getBaseContext(),HelpTopics.class);
                startActivity(intent_hp);
                return true;
            case R.id.menu_website:
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ebuymyanmar.com/"));
                startActivity(intent);
                return true;
            case R.id.menu_facebook:
                Intent intent_fb= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/ebuy.myanmar"));
                startActivity(intent_fb);
                return true;
            case R.id.menu_my_account:
                Intent ActivityMyAccount = new Intent(getBaseContext(),ActivityMyAccount.class);
                startActivity(ActivityMyAccount);
                return true;
            case R.id.menu_sell_your_item:
                Intent ActivitySellYourItems = new Intent(getBaseContext(),ActivitySellYourItems.class);
                startActivity(ActivitySellYourItems);
                return true;
            case R.id.menu_log_out:
                if (mPreferences.contains("BuyerId")) {
                    mPreferences.edit().clear().commit();
                    Toast.makeText(getBaseContext(), "Logging Out", Toast.LENGTH_SHORT).show();
                    navDrawerItems.get(13).setTitle("Sign In");
                }else {
                    Intent intent_sign_in = new Intent(getBaseContext(), SignInActivity.class);
                    startActivity(intent_sign_in);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(menu_slide);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new FragmentMain();
                break;
            case 1:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 2:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 3:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 4:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 5:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 6:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 7:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 8:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 9:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 10:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 11:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 12:
                fragment = new AuthenticBrandZoneFragment();
                break;
            case 13:
                fragment = new AuthenticBrandZoneFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(menu_slide);
        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}