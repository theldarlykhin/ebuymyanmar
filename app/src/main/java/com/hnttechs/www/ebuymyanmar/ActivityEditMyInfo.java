package com.hnttechs.www.ebuymyanmar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 12/17/16.
 */
public class ActivityEditMyInfo extends AppCompatActivity {

    EditText txt_phone;
    EditText txt_address;
    EditText txt_township;
    EditText txt_new_name;
    EditText txt_email;
    EditText txt_password;
    EditText txt_password_confirmation;

    Button btn_submit;

    ProgressDialog dialog;

    String website_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Product Detail");
        toolbar.setLogo(R.drawable.ebuy_menu_logo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txt_phone = (EditText)findViewById(R.id.txt_phone);
        txt_address = (EditText)findViewById(R.id.txt_address);
        txt_township = (EditText)findViewById(R.id.txt_township);
        txt_new_name = (EditText)findViewById(R.id.txt_new_name);
        txt_email = (EditText)findViewById(R.id.txt_email);
        txt_password = (EditText)findViewById(R.id.txt_password);
        txt_password_confirmation = (EditText)findViewById(R.id.txt_password_confirmation);

        btn_submit = (Button)findViewById(R.id.btn_submit);

        Resources resources = getResources();
        website_name = resources.getString(R.string.website_name);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = ProgressDialog.show(ActivityEditMyInfo.this, "",
                        "Sending...", true);
                dialog.show();
                new MyAsyncTask().execute(
                        "HtooMyat",
                        txt_phone.getText().toString(), txt_address.getText().toString(),
                        txt_township.getText().toString(),txt_email.getText().toString(),
                        txt_password.getText().toString(),txt_password_confirmation.getText().toString(),
                        website_name);
            }
        });
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, Double>{

        @Override
        protected Double doInBackground(String... params) {
            SendDataToServer.postData(params[0], params[1], params[2], params[3], params[4], params[5],
                    params[6], params[7], params[8]);
            return null;
        }

        protected void onPostExecute(Double result){
            Toast.makeText(getApplicationContext(), "command sent", Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }
        protected void onProgressUpdate(Integer... progress){
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }
}

