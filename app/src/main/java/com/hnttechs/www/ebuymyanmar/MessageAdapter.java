package com.hnttechs.www.ebuymyanmar;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jamiepatel on 22/09/2015.
 */
public class MessageAdapter extends BaseAdapter {

    Context messageContext;
    ArrayList<Message> messageList;
    static LinearLayout layout_get_message;
    static LinearLayout layout_send_message;

    public MessageAdapter(Context context, ArrayList<Message> messages) {
        messageList = messages;
        messageContext = context;
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Object getItem(int position) {
        return messageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageViewHolder holder;
        if (convertView == null) {
            LayoutInflater messageInflater = (LayoutInflater) messageContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = messageInflater.inflate(R.layout.message, null);
            holder = new MessageViewHolder();
            holder.getsenderView = (TextView) convertView.findViewById(R.id.get_message_sender);
            holder.getbodyView = (TextView) convertView.findViewById(R.id.get_message_body);
            holder.sendsenderView = (TextView) convertView.findViewById(R.id.send_message_sender);
            holder.sendbodyView= (TextView) convertView.findViewById(R.id.send_message_body);


            layout_get_message = (LinearLayout) convertView.findViewById(R.id.layout_get_message);
            layout_send_message = (LinearLayout) convertView.findViewById(R.id.layout_send_message);


            Message message = (Message) getItem(Chat.count);

            int my_name_length = Chat.username.length();


//            if (message.channels.equals(Chat.channel_me)) {

                holder.getsenderView.setText(message.channels.substring(0,
                        message.channels.length() - my_name_length) + ":");
                holder.getbodyView.setText(message.message);
//                layout_get_message.setVisibility(View.VISIBLE);
//                layout_send_message.setVisibility(View.GONE);
//
//            } else if (message.channels.equals(Chat.channel_send)) {
//                holder.sendsenderView.setText("me");
//                holder.sendbodyView.setText(message.message);
//                layout_send_message.setVisibility(View.VISIBLE);
//                layout_get_message.setVisibility(View.GONE);
//            }
            Chat.count++;


            convertView.setTag(holder);
        } else {
            holder = (MessageViewHolder) convertView.getTag();
        }


        return convertView;
    }

    public void add(Message message) {
        messageList.add(message);
        notifyDataSetChanged();
    }

    private static class MessageViewHolder {
        public ImageView thumbnailImageView;
        public TextView getsenderView;
        public TextView getbodyView;
        public TextView sendsenderView;
        public TextView sendbodyView;
    }
}
