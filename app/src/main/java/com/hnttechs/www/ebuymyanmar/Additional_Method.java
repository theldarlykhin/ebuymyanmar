package com.hnttechs.www.ebuymyanmar;

import android.content.Context;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dell on 12/12/16.
 */
public class Additional_Method {

    static List<String> SPINNER_DATA = new ArrayList<String>();

    public static void fill_data_to_spinner_with_arraylist(Context context, String spinnerdata, Spinner spinnerName) {

        SPINNER_DATA = Arrays.asList(spinnerdata.split(","));
        Zawgyi_SpinnerAdapter adapter = new Zawgyi_SpinnerAdapter(context, android.R.layout.simple_spinner_item, SPINNER_DATA);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerName.setAdapter(adapter);
    }

}
