package com.hnttechs.www.ebuymyanmar;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FragmentMain extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String[] navMenuTitles;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_main, container, false);

        viewPager = (ViewPager) rootview.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) rootview.findViewById(R.id.tabs);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout)
                    LayoutInflater.from(getActivity()).inflate(R.layout.tab_layout, tabLayout, false);

            TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tab_title);
            tabTextView.setText(tab.getText());
            tab.setCustomView(relativeLayout);
            tab.select();
        }
        viewPager.setCurrentItem(0);

        return rootview;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());

        Resources resources = getResources();
        navMenuTitles = resources.getStringArray(R.array.nav_drawer_items);

        adapter.addFragment(new AllCategoryFragment(), navMenuTitles[0]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[1]);
        adapter.addFragment(new WomensFashion(), navMenuTitles[2]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[3]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[4]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[5]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[6]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[7]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[8]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[9]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[10]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[11]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[12]);
        adapter.addFragment(new AuthenticBrandZoneFragment(), navMenuTitles[13]);
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
