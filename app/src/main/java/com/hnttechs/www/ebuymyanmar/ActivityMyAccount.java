package com.hnttechs.www.ebuymyanmar;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by dell on 12/17/16.
 */
public class ActivityMyAccount extends AppCompatActivity implements View.OnClickListener  {
    String website_name;
    HashMap<String, String> map;

    TextView txt_name;
    TextView txt_phone;
    TextView txt_address;
    TextView txt_township;
    TextView lbl_edit_my_info;
    TextView lbl_view_shopping_list;
    TextView lbl_view_wish_list;
    TextView lbl_view_list;
    TextView lbl_view_coupon_list;
    TextView lbl_view_cancel_refund_list;
    TextView lbl_view_order_list;

    ImageView img_edit_my_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Product Detail");
        toolbar.setLogo(R.drawable.ebuy_menu_logo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txt_name = (TextView)findViewById(R.id.txt_name);
        txt_phone = (TextView)findViewById(R.id.txt_phone);
        txt_address = (TextView)findViewById(R.id.txt_address);
        txt_township = (TextView)findViewById(R.id.txt_township);
        lbl_edit_my_info = (TextView)findViewById(R.id.lbl_edit_my_info);
        lbl_view_shopping_list = (TextView)findViewById(R.id.txt_view_shopping_list);
        lbl_view_wish_list= (TextView)findViewById(R.id.txt_view_wish_list);
        lbl_view_list= (TextView)findViewById(R.id.txt_view_list);
        lbl_view_coupon_list= (TextView)findViewById(R.id.txt_view_coupon_list);
        lbl_view_cancel_refund_list= (TextView)findViewById(R.id.txt_view_Cancel_refund_list);
        lbl_view_order_list= (TextView)findViewById(R.id.txt_view_my_order_list);

        img_edit_my_info = (ImageView)findViewById(R.id.img_edit_my_info);

        Resources resources = getResources();
        website_name = resources.getString(R.string.website_name);

        lbl_edit_my_info.setOnClickListener(this);
        img_edit_my_info.setOnClickListener(this);

        lbl_view_shopping_list.setOnClickListener(this);
        lbl_view_wish_list.setOnClickListener(this);
        lbl_view_list.setOnClickListener(this);
        lbl_view_coupon_list.setOnClickListener(this);
        lbl_view_cancel_refund_list.setOnClickListener(this);
        lbl_view_order_list.setOnClickListener(this);

        new DataFetcherTask().execute();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.lbl_edit_my_info:
                Intent intent_edit_my_info = new Intent(getBaseContext(),ActivityEditMyInfo.class);
                startActivity(intent_edit_my_info);
                break;
            case R.id.img_edit_my_info:
                Intent intent_edit_my_info_2 = new Intent(getBaseContext(),ActivityEditMyInfo.class);
                startActivity(intent_edit_my_info_2);
                break;
            case R.id.txt_view_shopping_list:
                Intent intent_shopping_list = new Intent(getBaseContext(),ActivityEditMyInfo.class);
                startActivity(intent_shopping_list);
                break;
            case R.id.txt_view_wish_list:
                Intent intent_wish_list= new Intent(getBaseContext(),ActivityEditMyInfo.class);
                startActivity(intent_wish_list);
                break;
            case R.id.txt_view_list:
                Intent intent_view_list= new Intent(getBaseContext(),ActivityEditMyInfo.class);
                startActivity(intent_view_list);
                break;
            case R.id.txt_view_coupon_list:
                Intent intent_coupon_list= new Intent(getBaseContext(),ActivityEditMyInfo.class);
                startActivity(intent_coupon_list);
                break;
            case R.id.txt_view_Cancel_refund_list:
                Intent intent_Cancel_refund_list= new Intent(getBaseContext(),ActivityEditMyInfo.class);
                startActivity(intent_Cancel_refund_list);
                break;
            case R.id.txt_view_my_order_list:
                Intent intent_order_list = new Intent(getBaseContext(),ActivityEditMyInfo.class);
                startActivity(intent_order_list);
                break;
        }
    }

    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            map = SyncDataFromServer.SelectMyInfo(website_name +
                    "/my_info.txt?buyer_name=" + "htoomyat");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            txt_name.setText(map.get("buyer_name"));
            txt_phone.setText(map.get("buyer_phone"));
            txt_township.setText(map.get("buyer_township"));
            txt_address.setText(map.get("buyer_address"));
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }
}
