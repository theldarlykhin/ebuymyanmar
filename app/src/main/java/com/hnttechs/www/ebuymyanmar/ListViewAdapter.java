package com.hnttechs.www.ebuymyanmar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/4/15.
 */
public class ListViewAdapter extends BaseAdapter {
    // Declare Variables
    Context context;
    ArrayList<HashMap<String, String>> grid_data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ImageLoader imageLoader = new ImageLoader(context);
    String website_name;

    public ListViewAdapter(Context c, ArrayList<HashMap<String, String>> griddata) {
        this.context = c;
        grid_data = griddata;
    }

    @Override
    public int getCount() {
        return grid_data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View gridView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            gridView = inflater.inflate(R.layout.homescreen_grid_item, null);


        } else {
            gridView = convertView;
        }
        resultp = grid_data.get(position);

        Resources resources = context.getResources();
        website_name = resources.getString(R.string.website_name);

            ImageView img_cloth_photo = (ImageView) gridView.findViewById(R.id.img_homescreen_icon);
            TextView lbl_price = (TextView) gridView.findViewById(R.id.txt_homescreen_price);
            TextView lbl_name = (TextView) gridView.findViewById(R.id.txt_homescreen_desc);

            lbl_name.setText(resultp.get("short_title"));
            lbl_price.setText(resultp.get("discount_price"));
            imageLoader.DisplayImage(website_name + resultp.get("avatar1"),img_cloth_photo);

        return gridView;
    }
}