package com.hnttechs.www.ebuymyanmar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dell on 11/21/16.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private ArrayList<NavDrawerItem> _listDataHeader;
    static String[] navMenuTitles;
    static int lastExpandedPosition =-1;
    static CustExpListview SecondLevelexplv;

    public ExpandableListAdapter(Context context, ArrayList<NavDrawerItem> listDataHeader) {
        this._context = context;
        this._listDataHeader = listDataHeader;



    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return childPosititon;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        SecondLevelexplv = new CustExpListview(_context);


        Resources resources = _context.getResources();
        if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Authentic Brand Zone")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_authentic_brand_zone);
        } else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Women's Fashion")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_woman_fashion);
        } else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Men's Fashion")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_man_fashion);
        } else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Digital & Mobile")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_digital_mobile_electronic);
        }else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Home & Living")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_home_living);
        }else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Health & Beauty")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_health_beauty);
        }else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Food & Beverage")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_food_beverage);
        }else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Kid, Baby and Maternity")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_kid_fashion);
        }else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Sports, Outdoors & Entertainment")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_sport);
        }else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Office & School Supplies")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_office_supplies);
        }else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Lights & Lightings")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_lighting);
        }else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Transportation")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_transportation);
        }else if(_listDataHeader.get(groupPosition).getTitle().toString().equals("Electronic, Electrical & Mechanic Products")) {
            navMenuTitles = resources.getStringArray(R.array.sub_menu_electronic);
        }

        if(navMenuTitles.length>0) {
            SecondLevelexplv.setAdapter(new ExpandableListAdapter_Third_level(_context, navMenuTitles,
                    _listDataHeader.get(groupPosition).getTitle().toString()));
            SecondLevelexplv.setGroupIndicator(null);
            SecondLevelexplv.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    if (lastExpandedPosition != -1
                            && groupPosition != lastExpandedPosition) {
                        SecondLevelexplv.collapseGroup(lastExpandedPosition);
                    }
                    lastExpandedPosition = groupPosition;
                }
            });
        }

        return SecondLevelexplv;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }


    @Override
    public int getGroupCount() {
        return _listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.title);


        ImageView img_icon = (ImageView)convertView.findViewById(R.id.img_icon);
        img_icon.setImageResource(_listDataHeader.get(groupPosition).getIcon());


//        lblListHeader.setText(navDrawerItems.get(position).getTitle());

        lblListHeader.setText(_listDataHeader.get(groupPosition).getTitle());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
