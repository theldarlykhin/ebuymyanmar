package com.hnttechs.www.ebuymyanmar;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dell on 5/4/16.
 */
public class Chat_ListAdapter extends BaseAdapter {


    // Declare Variables
    Context context;
    LayoutInflater inflater;
    static JSONObject json_data;

    public Chat_ListAdapter(Context context, String data) {
        this.context = context;
        try {
            json_data = new JSONObject(data);
        } catch (JSONException e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return json_data.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.chat_list_item, parent, false);
        }
        try {

            final TextView lbl_name = (TextView) convertView.findViewById(R.id.lbl_name);

            final TextView lbl_message = (TextView) convertView.findViewById(R.id.lbl_message);

            lbl_name.setText("yeminthein1");
            lbl_message.setText(json_data.getString("message"));
        } catch (JSONException e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        return convertView;
    }
}
