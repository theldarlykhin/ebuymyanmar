package com.hnttechs.www.ebuymyanmar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 4/14/17.
 */
public class ActivityCart extends AppCompatActivity implements View.OnClickListener {

    private FullLengthListView lv_cart;
    String website_name;

    public ArrayList<HashMap<String, String>> map;
    private SharedPreferences mPreferences;
    ListViewAdapter_cart lv_adapter_cart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);


        lv_cart= (FullLengthListView) findViewById(R.id.lv_cart);

        Resources resources = getResources();
        website_name = resources.getString(R.string.website_name);

        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);

        new DataFetcherTask().execute();

        lv_cart.setFocusable(false);

    }

    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            if (mPreferences.contains("BuyerId")) {
                map = SyncDataFromServer.GetCart(website_name +
                        "/mycart_list_by_buyer_id.txt?buyer_id=" + mPreferences.getString("BuyerId", ""));
            }else {
                map = SyncDataFromServer.GetCart(website_name +
                        "/mycart_list_by_cart_key.txt?cart_key=" + mPreferences.getString("cart_key", ""));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            lv_adapter_cart = new ListViewAdapter_cart(getBaseContext(), map);
            lv_cart.setAdapter(lv_adapter_cart);

        }
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_check_out:
                Intent checkout_intent = new Intent(getBaseContext(), ActivityCheckOut.class);
                startActivity(checkout_intent);
                break;
        }
    }
}
