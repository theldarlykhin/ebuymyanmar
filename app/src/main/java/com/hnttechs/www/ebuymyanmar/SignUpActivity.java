package com.hnttechs.www.ebuymyanmar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by dell on 10/4/16.
 */
public class SignUpActivity extends Activity {


    RelativeLayout relativelayout;
    RelativeLayout username_stroke;
    RelativeLayout password_stroke;
    RelativeLayout email_stroke;
    RelativeLayout phone_stroke;
    RelativeLayout township_stroke;
    RelativeLayout address_stroke;
    ImageView img_icon;
    ImageView username_icon;
    ImageView password_icon;
    ImageView email_icon;
    ImageView phone_icon;
    ImageView township_icon;
    ImageView address_icon;
    EditText txt_username;
    EditText txt_password;
    EditText txt_email;
    EditText txt_phone;
    EditText txt_township;
    EditText txt_address;
    Button btn_sign_up;
    RelativeLayout.LayoutParams relativeLayoutParams;
    RelativeLayout.LayoutParams img_iconLayoutParams;
    Drawable mDrawable_username_icon_red;
    Drawable mDrawable_username_icon_gray;
    Drawable mDrawable_password_icon_red;
    Drawable mDrawable_password_icon_gray;
    Drawable mDrawable_email_icon_red;
    Drawable mDrawable_email_icon_gray;
    Drawable mDrawable_phone_icon_red;
    Drawable mDrawable_phone_icon_gray;
    Drawable mDrawable_township_icon_red;
    Drawable mDrawable_township_icon_gray;
    Drawable mDrawable_address_icon_red;
    Drawable mDrawable_address_icon_gray;
    ProgressDialog dialog;
    String website_name;
    private static String memail;
    private static String mpassword;
    private static String mbuyer_name;
    private static String mbuyer_phone;
    private static String mbuyer_township;
    private static String mbuyer_address;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        relativelayout = (RelativeLayout)findViewById(R.id.relativeLayout);
        username_stroke = (RelativeLayout)findViewById(R.id.username_stroke);
        password_stroke = (RelativeLayout)findViewById(R.id.password_stroke);
        email_stroke = (RelativeLayout)findViewById(R.id.email_stroke);
        phone_stroke = (RelativeLayout)findViewById(R.id.phone_stroke);
        township_stroke= (RelativeLayout)findViewById(R.id.township_stroke);
        address_stroke= (RelativeLayout)findViewById(R.id.address_stroke);
        img_icon = (ImageView)findViewById(R.id.img_icon);
        username_icon = (ImageView)findViewById(R.id.username_icon);
        password_icon = (ImageView)findViewById(R.id.password_icon);
        txt_username = (EditText)findViewById(R.id.txt_username);
        txt_password = (EditText)findViewById(R.id.txt_password);
        email_icon = (ImageView)findViewById(R.id.email_icon);
        phone_icon = (ImageView)findViewById(R.id.phone_icon);
        txt_email = (EditText)findViewById(R.id.txt_email);
        txt_phone = (EditText)findViewById(R.id.txt_phone);
        township_icon = (ImageView)findViewById(R.id.township_icon);
        address_icon= (ImageView)findViewById(R.id.address_icon);
        txt_township = (EditText)findViewById(R.id.txt_township);
        txt_address= (EditText)findViewById(R.id.txt_address);
        btn_sign_up = (Button)findViewById(R.id.btn_sign_up);


        mDrawable_username_icon_red = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
        mDrawable_username_icon_red.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));

        mDrawable_username_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
        mDrawable_username_icon_gray.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


        mDrawable_password_icon_red = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
        mDrawable_password_icon_red.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));

        mDrawable_password_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
        mDrawable_password_icon_gray.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));



        mDrawable_email_icon_red= getBaseContext().getResources().getDrawable(R.drawable.email);
        mDrawable_email_icon_red.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));

        mDrawable_email_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.email);
        mDrawable_email_icon_gray.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));



        mDrawable_phone_icon_red = getBaseContext().getResources().getDrawable(R.drawable.full_name);
        mDrawable_phone_icon_red.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));

        mDrawable_phone_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.full_name);
        mDrawable_phone_icon_gray.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));

        mDrawable_township_icon_red= getBaseContext().getResources().getDrawable(R.drawable.email);
        mDrawable_township_icon_red.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));

        mDrawable_township_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.email);
        mDrawable_township_icon_gray.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));



        mDrawable_address_icon_red = getBaseContext().getResources().getDrawable(R.drawable.full_name);
        mDrawable_address_icon_red.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));

        mDrawable_address_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.full_name);
        mDrawable_address_icon_gray.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));

        username_icon.setImageDrawable(mDrawable_username_icon_gray);
        password_icon.setImageDrawable(mDrawable_password_icon_gray);
        email_icon.setImageDrawable(mDrawable_email_icon_gray);
        phone_icon.setImageDrawable(mDrawable_phone_icon_gray);
        township_icon.setImageDrawable(mDrawable_township_icon_gray);
        address_icon.setImageDrawable(mDrawable_address_icon_gray);

        username_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
        password_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
        email_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
        phone_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
        township_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
        address_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

        Resources resources = getResources();
        website_name = resources.getString(R.string.website_name);

        txt_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                mDrawable_username_icon_red = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
                mDrawable_username_icon_red.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));
                username_icon.setImageDrawable(mDrawable_username_icon_red);
                username_stroke.setBackgroundColor(Color.parseColor("#ee1f25"));

                mDrawable_password_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
                mDrawable_password_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                password_icon.setImageDrawable(mDrawable_password_icon_gray);
                password_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_email_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_email_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                email_icon.setImageDrawable(mDrawable_email_icon_gray);
                email_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_phone_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_phone_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                phone_icon.setImageDrawable(mDrawable_phone_icon_gray);
                phone_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_township_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_township_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                township_icon.setImageDrawable(mDrawable_township_icon_gray);
                township_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_address_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_address_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                address_icon.setImageDrawable(mDrawable_address_icon_gray);
                address_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));


            }
        });


        txt_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                mDrawable_password_icon_red = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
                mDrawable_password_icon_red.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));
                password_icon.setImageDrawable(mDrawable_password_icon_red);

                password_stroke.setBackgroundColor(Color.parseColor("#ee1f25"));

                mDrawable_username_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
                mDrawable_username_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                username_icon.setImageDrawable(mDrawable_username_icon_gray);

                username_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));


                mDrawable_email_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_email_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                email_icon.setImageDrawable(mDrawable_email_icon_gray);

                email_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));


                mDrawable_phone_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_phone_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                phone_icon.setImageDrawable(mDrawable_phone_icon_gray);

                phone_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_township_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_township_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                township_icon.setImageDrawable(mDrawable_township_icon_gray);
                township_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_address_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_address_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                address_icon.setImageDrawable(mDrawable_address_icon_gray);
                address_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
            }
        });

        txt_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mDrawable_password_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
                mDrawable_password_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                password_icon.setImageDrawable(mDrawable_password_icon_gray);

                password_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_username_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
                mDrawable_username_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                username_icon.setImageDrawable(mDrawable_username_icon_gray);

                username_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));


                mDrawable_email_icon_red= getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_email_icon_red.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));

                email_icon.setImageDrawable(mDrawable_email_icon_red);

                email_stroke.setBackgroundColor(Color.parseColor("#ee1f25"));

                mDrawable_phone_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_phone_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                phone_icon.setImageDrawable(mDrawable_phone_icon_gray);

                phone_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_township_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_township_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                township_icon.setImageDrawable(mDrawable_township_icon_gray);
                township_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_address_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_address_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                address_icon.setImageDrawable(mDrawable_address_icon_gray);
                address_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
            }
        });



        txt_phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mDrawable_password_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
                mDrawable_password_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                password_icon.setImageDrawable(mDrawable_password_icon_gray);

                password_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_username_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
                mDrawable_username_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                username_icon.setImageDrawable(mDrawable_username_icon_gray);

                username_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));


                mDrawable_email_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_email_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));

                email_icon.setImageDrawable(mDrawable_email_icon_gray);

                email_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_phone_icon_red = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_phone_icon_red.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));


                phone_icon.setImageDrawable(mDrawable_phone_icon_red);

                phone_stroke.setBackgroundColor(Color.parseColor("#ee1f25"));

                mDrawable_township_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_township_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                township_icon.setImageDrawable(mDrawable_township_icon_gray);
                township_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_address_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_address_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                address_icon.setImageDrawable(mDrawable_address_icon_gray);
                address_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
            }
        });


        txt_township.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mDrawable_password_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
                mDrawable_password_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                password_icon.setImageDrawable(mDrawable_password_icon_gray);

                password_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_username_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
                mDrawable_username_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                username_icon.setImageDrawable(mDrawable_username_icon_gray);

                username_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));


                mDrawable_email_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_email_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));

                email_icon.setImageDrawable(mDrawable_email_icon_gray);

                email_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_phone_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_phone_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                phone_icon.setImageDrawable(mDrawable_phone_icon_gray);

                phone_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_township_icon_red = getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_township_icon_red.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));
                township_icon.setImageDrawable(mDrawable_township_icon_red);
                township_stroke.setBackgroundColor(Color.parseColor("#ee1f25"));

                mDrawable_address_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_address_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                address_icon.setImageDrawable(mDrawable_address_icon_gray);
                address_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));
            }
        });


        txt_address.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mDrawable_password_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.password_icon);
                mDrawable_password_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                password_icon.setImageDrawable(mDrawable_password_icon_gray);

                password_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_username_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.username_icon);
                mDrawable_username_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));


                username_icon.setImageDrawable(mDrawable_username_icon_gray);

                username_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));


                mDrawable_email_icon_gray= getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_email_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));

                email_icon.setImageDrawable(mDrawable_email_icon_gray);

                email_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_phone_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_phone_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));

                phone_icon.setImageDrawable(mDrawable_phone_icon_gray);

                phone_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_township_icon_gray = getBaseContext().getResources().getDrawable(R.drawable.email);
                mDrawable_township_icon_gray.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#d3d3d3"), PorterDuff.Mode.MULTIPLY));
                township_icon.setImageDrawable(mDrawable_township_icon_gray);
                township_stroke.setBackgroundColor(Color.parseColor("#d3d3d3"));

                mDrawable_address_icon_red = getBaseContext().getResources().getDrawable(R.drawable.full_name);
                mDrawable_address_icon_red.setColorFilter(new
                        PorterDuffColorFilter(Color.parseColor("#ee1f25"), PorterDuff.Mode.MULTIPLY));
                address_icon.setImageDrawable(mDrawable_address_icon_red);
                address_stroke.setBackgroundColor(Color.parseColor("#ee1f25"));
            }
        });

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                memail = txt_email.getText().toString();
                mpassword = txt_password.getText().toString();
                mbuyer_name = txt_username.getText().toString();
                mbuyer_phone = txt_phone.getText().toString();
                mbuyer_township = txt_township.getText().toString();
                mbuyer_address = txt_address.getText().toString();
                RegisterTask registerTask = new RegisterTask(SignUpActivity.this);
                registerTask.setMessageLoading("Registering new account...");
                registerTask.execute(website_name + "/myapi/v1/buyer_new");
            }
        });
    }

    private class RegisterTask extends UrlJsonAsyncTask {
        public RegisterTask(Context context) {
            super(context);
        }

        @Override
        protected JSONObject doInBackground(String... urls) {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urls[0]);
            JSONObject holder = new JSONObject();
            JSONObject userObj = new JSONObject();
            String response = null;
            JSONObject json = new JSONObject();

            try {
                try {
                    json.put("success", false);

                    json.put("info", "Sign up SUCCESSFULLY.Please sign in to continue.");
                    userObj.put("email", memail);
                    userObj.put("password", mpassword);
                    userObj.put("buyer_name", mbuyer_name);
                    userObj.put("buyer_phone", mbuyer_phone);
                    userObj.put("buyer_township", mbuyer_township);
                    userObj.put("buyer_address", mbuyer_address);

                    StringEntity se = new StringEntity(userObj.toString());
                    post.setEntity(se);

                    post.setHeader("Accept", "application/json");
                    post.setHeader("Content-Type", "application/json");

                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    response = client.execute(post, responseHandler);
                    json = new JSONObject(response);

                } catch (HttpResponseException e) {
                    e.printStackTrace();
                    Log.e("ClientProtocol", "" + e);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("IO", "" + e);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("JSON", "" + e);
            }

            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            finish();
            try {
                if (json.get("success").toString() == "true") {

                    Toast.makeText(context, "Successfully register", Toast.LENGTH_LONG).show();
                } else if (json.get("success").toString() == "false" ){
                    Toast.makeText(getBaseContext(), "Fail to register", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            } finally {
                super.onPostExecute(json);
            }
        }
    }
}
