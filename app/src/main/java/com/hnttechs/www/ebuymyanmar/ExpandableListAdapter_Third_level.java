package com.hnttechs.www.ebuymyanmar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dell on 11/21/16.
 */
public class ExpandableListAdapter_Third_level extends BaseExpandableListAdapter {

    private Context _context;
    private String[] title;
    static String[] navMenuTitles;
    static String group_title;

    static int lastExpandedPosition = -1;
    static CustExpListview SecondLevelexplv;
    HashMap<String, List<String>> listDataChild;

    public ExpandableListAdapter_Third_level(Context context, String[] title, String group_title) {
        this._context = context;
        this.title = title;
        this.group_title = group_title;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return childPosititon;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        SecondLevelexplv = new CustExpListview(_context);

        Resources resources = _context.getResources();

        if (group_title.equals("Authentic Brand Zone")) {
            if (title[groupPosition].toString().equals("All Products on Authentic Brand Zone")) {
                Intent i = new Intent(_context, ActivityAuthenticBrandZone.class);
                _context.startActivity(i);
            } else if (title[groupPosition].toString().equals("Women's Fashion")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_woman_fashion);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Men's Fashion")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_man_fashion);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Digital & Mobile")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_digital_mobile_electronic);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Home & Living")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_home_living);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Health & Beauty")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_health_beauty);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Food & Beverate")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_food_beverage);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Kid, Baby and Maternity")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_kid_fashion);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Sports, Outdoors & Entertainment")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_sport);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Office & School Supplies")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_office_supplies);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Lights & Lightings")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_lighting);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Transportation")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_transportation);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Electronic, Electrical & Mechanic Products")) {
                navMenuTitles = resources.getStringArray(R.array.authentic_sub_menu_electronic);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            }
        } else if (group_title.equals("Women's Fashion")) {
            if (title[groupPosition].toString().equals("Women's Apparel")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_women_apparel);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Shoes")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_shoes_footwear);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Bags & Wallets")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_bag_wallets);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Watches")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_watch_glass);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Jewellery")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_jewellery);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Men's Fashion")) {
            if (title[groupPosition].toString().equals("Man's Apparel")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_mens_apparel);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Shoes")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_men_shoe_footwear);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Bags & Wallets")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_men_bag_wallets);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Watches")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_men_watches_glasses);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Jewellery")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_men_jewellery);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Digital & Mobile")) {
            if (title[groupPosition].toString().equals("Phones and Tables")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_phone_tablets);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Computers and Laptops")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_computer_laptops);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("TV, Video and Player")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_video_player);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Audio and Hi Fi")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_audio_hi_fi);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Camera and Photo")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_camera_photos);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Gaming")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_gaming);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Home Appliances")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_home_electrical_appliances);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }

        } else if (group_title.equals("Home & Living")) {
            if (title[groupPosition].toString().equals("Living Room Supplies")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_living_room_appliances);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Bedroom Appliances")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_bedroom_products);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Bathroom Products")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_bathroom_appliances);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Garment Care")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_garment_care);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Kitchen and Dining")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_kitchen_dining);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Housekeeping")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_house_keeping);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Home Decoration")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_home_decoration);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Outdoor and Garden")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_outdoor_garden);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Health & Beauty")) {

            if (title[groupPosition].toString().equals("Bath and Body Supplies")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_bathcare_bodycare);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Skin Care")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_skincare);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Eye Care, Hair Care, Hand Care")) {

                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_eye_care);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Dental Care")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_dental_care);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Perfumes")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_perfumes);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Shaving Supplies")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_shaving_supplies);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Food Supplements")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_food_supplements);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Health and Beauty Tools")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_health_beauty_accessories);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Food & Beverage")) {
            if (title[groupPosition].toString().equals("Vegetable Products")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_vegetable_products);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Confectionery")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_confectionery);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Baked Goods")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_baked_goods);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Grain Products")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_grain_products);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Food Ingredients")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_food_ingredients);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Dairy Products")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_dairy_products);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Honey Products")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_honey_products);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Meat and Meat Products")) {
            } else if (title[groupPosition].toString().equals("Egg and Egg Products")) {
            } else if (title[groupPosition].toString().equals("Seafood and Seafood Products")) {
            } else if (title[groupPosition].toString().equals("Coffee")) {
            } else if (title[groupPosition].toString().equals("Instant Food")) {
            } else if (title[groupPosition].toString().equals("Canned Food")) {
            } else if (title[groupPosition].toString().equals("Baby Food")) {
            } else if (title[groupPosition].toString().equals("Beverage")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_beverage);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Kid, Baby and Maternity")) {
            if (title[groupPosition].toString().equals("Kid\'s Fashion")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_kids_fashion);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Baby Care")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_baby_care);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Baby Feeding")) {
            } else if (title[groupPosition].toString().equals("Toys")) {
            } else if (title[groupPosition].toString().equals("Maternity Care")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_maternity_care);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Sports, Outdoors & Entertainment")) {
            if (title[groupPosition].toString().equals("Indoor Sport Supplies")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_indoor_sport_supplies);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Outdoor Sport Supplies")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_outdoor_sport_supplies);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Outdoor Recreation")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_outdoor_recreation);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Entertainment")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_entertainment);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Office & School Supplies")) {
            if (title[groupPosition].toString().equals("Education Supplies")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_education_supplies);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Office Supplies")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_office_supplies);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Art Supplies")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_art_supplies);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Lights & Lightings")) {
            if (title[groupPosition].toString().equals("Indoor Lightings")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_indoor_lighting);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Outdoor Lightings")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_outdoor_lighting);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Transportation")) {
            if (title[groupPosition].toString().equals("Bicycles")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_bicycles);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Automobiles and Motorcycles")) {
                navMenuTitles = resources.getStringArray(R.array.thid_sub_menu_automobiles_motocycles);
                SecondLevelexplv.setAdapter(new ExpandableListAdapter_fourth_level(_context, navMenuTitles));
                SecondLevelexplv.setGroupIndicator(null);
            } else if (title[groupPosition].toString().equals("Others")) {
            }
        } else if (group_title.equals("Electronic, Electrical & Mechanic Products")) {
            if (title[groupPosition].toString().equals("Electronic Components and Parts")) {
            } else if (title[groupPosition].toString().equals("Electrical Equipments and Parts")) {
            } else if (title[groupPosition].toString().equals("Mechanical Products and Tools")) {
            } else if (title[groupPosition].toString().equals("Safety Equipments and Accessories")) {
            } else if (title[groupPosition].toString().equals("OEM Products")) {
            }
        }
        return SecondLevelexplv;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }


    @Override
    public int getGroupCount() {
        return title.length;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.title);

        lblListHeader.setText(title[groupPosition]);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
