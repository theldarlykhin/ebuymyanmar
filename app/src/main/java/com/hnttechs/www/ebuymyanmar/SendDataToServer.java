package com.hnttechs.www.ebuymyanmar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dell on 12/10/16.
 */
public class SendDataToServer {
    static String response;

    public static void postData(String name,String new_name,String phone,String township,String address,
                         String email, String password,String password_confirmation,String website_name) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(website_name + "/edit_my_info?");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("buyer_name", name));
            nameValuePairs.add(new BasicNameValuePair("new_buyer_name", new_name));
            nameValuePairs.add(new BasicNameValuePair("new_buyer_phone", phone));
            nameValuePairs.add(new BasicNameValuePair("new_buyer_township", township));
            nameValuePairs.add(new BasicNameValuePair("new_buyer_address",address ));
            nameValuePairs.add(new BasicNameValuePair("email", email));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            nameValuePairs.add(new BasicNameValuePair("password_confirmation", password_confirmation));

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);

        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
    }


    public static void postReview(String product_name,String buyer_comment,String product_id,
                                  String buyer_name,String website_name) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(website_name + "/customer_review?");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("product_name", product_name));
            nameValuePairs.add(new BasicNameValuePair("buyer_comment", buyer_comment));
            nameValuePairs.add(new BasicNameValuePair("product_id", product_id));
            nameValuePairs.add(new BasicNameValuePair("buyer_name", buyer_name));

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);

        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
    }


    public static void add_to_cart(String website_name, String product_id,String product_size,String product_color,
                                  String delivery_method,String product_quantity, String product_model, String BuyerID) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(website_name);



        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("product_id", product_id));
            nameValuePairs.add(new BasicNameValuePair("product_size", product_size));
            nameValuePairs.add(new BasicNameValuePair("product_color", product_color));
            nameValuePairs.add(new BasicNameValuePair("delivery_method", delivery_method));
            nameValuePairs.add(new BasicNameValuePair("product_quantity", product_quantity));
            nameValuePairs.add(new BasicNameValuePair("product_model", product_model));
            nameValuePairs.add(new BasicNameValuePair("buyer_id",BuyerID));

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);

        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
    }

    public static String add_to_cart_with_key(String website_name, String product_id,String product_size,String product_color,
                                   String delivery_method,String product_quantity, String product_model, String cart_key) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(website_name);

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("product_id", product_id));
            nameValuePairs.add(new BasicNameValuePair("product_size", product_size));
            nameValuePairs.add(new BasicNameValuePair("product_color", product_color));
            nameValuePairs.add(new BasicNameValuePair("delivery_method", delivery_method));
            nameValuePairs.add(new BasicNameValuePair("product_quantity", product_quantity));
            nameValuePairs.add(new BasicNameValuePair("product_model", product_model));
            nameValuePairs.add(new BasicNameValuePair("cart_key", cart_key));

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            response =  httpclient.execute(httppost, responseHandler);

        } catch (ClientProtocolException e) {
            response = "";
        } catch (IOException e) {
            response = "";
        }
        return  response;
    }

    public static void postOrderForm(String product_name,String buyer_comment,String product_id,
                                  String buyer_name,String website_name) {
//        HttpClient httpclient = new DefaultHttpClient();
//        HttpPost httppost = new HttpPost(website_name + "/customer_review?");
//
//        try {
//            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//            nameValuePairs.add(new BasicNameValuePair("product_name", product_name));
//            nameValuePairs.add(new BasicNameValuePair("buyer_comment", buyer_comment));
//            nameValuePairs.add(new BasicNameValuePair("product_id", product_id));
//            nameValuePairs.add(new BasicNameValuePair("buyer_name", buyer_name));
//
//            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//
//            HttpResponse response = httpclient.execute(httppost);
//
//        } catch (ClientProtocolException e) {
//        } catch (IOException e) {
//        }
    }
}