package com.hnttechs.www.ebuymyanmar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/31/16.
 */
public class Message_Box_ListAdapter extends BaseAdapter {

    // Declare Variables
    Context c;
    ArrayList<HashMap<String, String>> serverdata_arraylist;
    HashMap<String, String> resultp = new HashMap<String, String>();

    public Message_Box_ListAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        c = context;
        serverdata_arraylist = arraylist;
    }

    @Override
    public int getCount() {
        return serverdata_arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.message_box_listitem, parent, false);
            TextView txt_user_name = (TextView)convertView.findViewById(R.id.txt_user_name);
            TextView txt_message = (TextView)convertView.findViewById(R.id.txt_message);
            resultp = serverdata_arraylist.get(position);

            txt_user_name.setText(resultp.get("user_id"));
            txt_message.setText(resultp.get("message"));

        }
        return convertView;
    }
}
